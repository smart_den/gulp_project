git clone https://smart_den@bitbucket.org/smart_den/gulp_project.git

1. npm init
2. npm i gulp --save-dev

3. Создаем структуру папок
    app - для разработки
    dist - продакшн

    app/css
    app/img
    app/fonts
    app/sass
    app/js

4. В корне создаем файл gulpfile.js
    **********************************************
    var gulp = require('gulp'); //подключаем галп
    //задаем таск
    gulp.task('mytask', function(){
      console.log('Привет, я таск!');
    });
    **************************************************
    //вызываем таск в консоли
    gulp mytask







gulpfile.js >>>>>>>>

var gulp        = require('gulp');
var sass        = require('gulp-sass');
var browserSync = require('browser-sync');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglifyjs');
var cssnano     = require('gulp-cssnano');
var rename      = require('gulp-rename');
var del         = require('del');
var imagemin    = require('gulp-imagemin');
var pngquant    = require('imagemin-pngquant');
var cache       = require('gulp-cache');
var autoprefixer = require('gulp-autoprefixer');

// gulp.task('mytask', function(){
//   console.log('Привет, я таск!');
// });

gulp.task('mytask', function(){
  // src -выбирает файлы
  // .pipe(plugin())   - вызов какого-то плагина, действия над файлом
  //  gulp.dest('folder') - папка куда выгружаем результат
  return gulp.src('source-files')
  .pipe(plugin())
  .pipe(gulp.dest('folder'))
});

gulp.task('sass', function(){
  return gulp.src('app/sass/main.sass')
  .pipe(sass())
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({stream: true}))
})

gulp.task('sass-all', function(){
  //шаблоны выборки
  // 'app/sass/**/*.sass' - выборка всех файлов .sass во всех директориях внутри папки app/sass/
  // return gulp.src('!app/sass/main.sass')  - !-исключает данный файл из выборки
  // return gulp.src(['!app/sass/main.sass', 'app/sass/**/*.sass'])  - выбрать все файлы sass кроме main.sass
  // return gulp.src('app/sass/*.+(scss|sass)')  - выбрать все scss и sass файлы
  return gulp.src('app/sass/**/*.sass')
  .pipe(sass())
  .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true} ))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({stream: true}))
})

//таск для сборки и сжатия скриптов (сожмем либы jquery и magnific-popup)
gulp.task('scripts', function(){
    return gulp.src([
      'app/libs/jquery/dist/jquery.min.js',
      'app/libs/magnific-popup/jquery.magnific-popup.min.js',
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/js'))
});

//таск для сборки и сжатия стилей в либах
gulp.task('css-libs', function(){
    return gulp.src('app/css/libs.css')
    .pipe(cssnano())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/css'))
});



// livereload   browser-sync
// npm i browser-sync --save-dev
// var browserSync = require('browser-sync');
//таск для browser-sync
gulp.task('browser-sync', function(){
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false
  });
});


// таск удаляет папку dist
gulp.task('clean', function(){
  return del.sync('dist');
});

// таск для очистки кеша  - прописывается вручную когда зависнет кеш
gulp.task('clear', function(){
  return cache.clearAll();
});

// таск оптимизации картинок
gulp.task('img', function(){
  return gulp.src('app/img/**/*')
        .pipe(cache(imagemin({
          interlaced: true,
          progressive: true,
          svgoPlugins: [{removeViewBox : false}],
          use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'))
});




// слежение за изменением файлов    gulp watch
// gulp.watch('директория файлов отслеживания', ['таска']);
// запускаем таску watch и файлы автоматом изменяются и отображается в консоли измениение
gulp.task('watch', ['browser-sync', 'sass-all', 'css-libs', 'scripts'], function(){
  gulp.watch('app/sass/**/*.sass', ['sass-all']);
  gulp.watch('app/*.html', browserSync.reload);  // отслеживание изменений в html и лайв релоад
  gulp.watch('app/js/**/*.js', browserSync.reload);  // отслеживание изменений в js и лайв релоад
})


// таск который переносит стили.фонты.скрипты из app в dist
gulp.task('build', ['clean', 'img', 'sass-all', 'css-libs', 'scripts'], function(){

  var buildCSS = gulp.src([
        'app/css/main.css',
        'app/css/libs.min.css',
      ])
      .pipe(gulp.dest('dist/css'));

  var buildFonts = gulp.src('app/fonts/**/*')
      .pipe(gulp.dest('dist/fonts'));

  var buildJs = gulp.src('app/js/**/*')
      .pipe(gulp.dest('dist/js'));

  var buildhtml = gulp.src('app/*.html')
      .pipe(gulp.dest('dist'));

})


//http://localhost:3000/   - наш сайт
//http://localhost:3001/   - настройки browserSync


//Также сжатие необходимо для скачиваемых библиотек в папке app/libs
// библиотеки ставим через bower
//   npm i -g bower

// Настроим BOWER
// в корне проекта создадим файл   .bowerrc
// {
//  "directory": "app/libs/",
// }

// установим jquery  и magnific-popup
//  bower i jquery magnific-popup --allow-root

// установим gulp-concet для конкатенации и gulp-uglifyjs сжимающий js
// npm i gulp-concat gulp-uglifyjs --save-dev

//подключаем библиотеки в gulpfile.js
//  var concat      = require('gulp-concat');
//  var uglify      = require('gulp-uglifyjs');
// таск для сборки и сжатия скриптов
//  gulp.task('scripts', function(){ ....

// gulp scripts
// в папке js появится файл libs.min.js



// дальше подключаем стили
// создаем файл app/sass/libs.sass
// в него делаем импорт стилей из magnific-popup
// @import "app/libs/magnific-popup/dist/magnific-popup"
// т.к. импортим css в sass то в конче можно не писать .css
// запускаем gulp sass-all и получаем файл libs.css в app/css


// теперь сожмем стили
// npm i gulp-cssnano gulp-rename --save-dev
//   var cssnano     = require('gulp-cssnano');
//   var rename     = require('gulp-rename');
// создадим таск gulp.task('css-libs', function(){,,,
// gulp.task('watch', ['browser-sync', 'sass-all', 'css-libs', 'scripts'], function(){
// в index.html
//    <link rel="stylesheet" href="css/libs.min.css">
//    <link rel="stylesheet" href="css/main.css">
//



// в app/img добавим 3 картинки

// И теперь самое главное  СБОРКА ПРОЕКТА !!!!!
// Создаем таску  gulp.task('build', function(){ ,,,
// перед началом сборки мы должны очистить все папки. для этого ставим пакет del
// npm i del --save-dev
// Перед watch создаем таску gulp.task('clean', function(){,,,
// которая удаляет папку dist
// далее
// gulp.task('build', ['clean', 'sass-all', 'css-libs', 'scripts'], function(){,,,
// gulp build собирает проект





// далее автоматическая оптимизация изображений
// npm i --save-dev gulp-imagemin imagemin-pngquant
// var imagemin    = require('gulp-imagemin');
// var pngquant    = require('imagemin-pngquant');
// таск gulp.task('img', function(){ ....
//gulp.task('build', ['clean', 'img', 'sass-all', 'css-libs', 'scripts'], function(){....
// gulp build


// т.к. картинки долго сжимаются нужно добавить к ним кеш
// npm i gulp-cache --save-dev
// var cache       = require('gulp-cache');
// добавим cache в таску
// gulp.task('img', function(){
//   return gulp.src('app/img/**/*')
//        .pipe(cache(imagemin({
//          interlaced: true,
//          progressive: true,
//          svgoPlugins: [{removeViewBox : false}],
//          use: [pngquant()]
//        })))
//        .pipe(gulp.dest('dist/img'))
// });

// но у нас будет пробадать сжатие картинок, потому нужен таск для очистки кеша
// gulp.task('clear', function(){


// Добавление автоматических вендорных префиксов
// npm i --save-dev gulp-autoprefixer
// var autoprefixer = require('gulp-autoprefixer');

// gulp.task('sass-all', function(){
//  return gulp.src('app/sass/**/*.sass')
//  .pipe(sass())
//  .pipe(autoprefixer(['last 15 versions']))
//  .pipe(gulp.dest('app/css'))
//  .pipe(browserSync.reload({stream: true}))
// })


// теперь в main.sass добавим body display: flex
// и в main.css появятся префиксы )))))




    /* Пример работы sass

    1a. npm i gulp-sass --save-dev
    2a. в gulpfile.js
       var sass = require('gulp-sass');
    3a. создадим app/sass/main.sass
        body
          background-color: blue

    4a. пишем таску для галпа
        gulp.task('sass', function(){
          return gulp.src('app/sass/main.sass')
          .pipe(sass())
          .pipe(gulp.dest('app/css/main.css'))
        })
    5a. вызываем в консоли gulp sass

        */



gulp.task('sass-all', function(){
  //шаблоны выборки
  // 'app/sass/**/*.sass' - выборка всех файлов .sass во всех директориях внутри папки app/sass/
  // return gulp.src('!app/sass/main.sass')  - !-исключает данный файл из выборки
  // return gulp.src(['!app/sass/main.sass', 'app/sass/**/*.sass'])  - выбрать все файлы sass кроме main.sass
  // return gulp.src('app/sass/*.+(scss|sass)')  - выбрать все scss и sass файлы
  return gulp.src('app/sass/**/*.sass')
  .pipe(sass())
  .pipe(gulp.dest('app/css'))
})
